FROM node:16

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./
RUN yarn

COPY ./ ./
ENV PORT=8545
EXPOSE 8545
CMD ["node", "src/index.js"]