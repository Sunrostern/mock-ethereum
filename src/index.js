'use strict';

const app = require('express')();

const port = process.env.PORT || 3000;

app.get('/', (request, response) => {
  response.send('Ethereum');
});

app.get('/hello', (request, response) => {
  response.send('Hello, Ethereum!');
});

app.listen(port, () => {
  console.log(`App listening at Port ${port}.`);
});
